import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpinrComponent } from './spinr.component';

describe('SpinrComponent', () => {
  let component: SpinrComponent;
  let fixture: ComponentFixture<SpinrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpinrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpinrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
