import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { PlayListComponent } from './play-list/play-list.component';
import { SpinrComponent } from './spinr/spinr.component';
import { ExperimentComponent } from './experiment/experiment.component';




const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'playlist', component: PlayListComponent },
  { path: 'spinr', component: SpinrComponent },
  { path: 'experiment', component: ExperimentComponent },



  { path: '',   redirectTo: '/home', pathMatch: 'full' }, // redirect to `first-component`
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
