import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-spinr',
  templateUrl: './spinr.component.html',
  styleUrls: ['./spinr.component.scss']
})
export class SpinrComponent implements OnInit {

  maSaUkazat = false;
  poleChruntov = [
    { id: 1, txt: 'ferferg' },
    { id: 2, txt: 'ferferrg' },
    { id: 3, txt: 'ferftgerg' },
    { id: 4, txt: 'ferfer7g' },
    { id: 5, txt: 'ferf876erg' },
    { id: 6, txt: 'ferf67erg' },
    { id: 7, txt: 'ferf7erg' },
    { id: 8, txt: 'fer5ferg' },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
